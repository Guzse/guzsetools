import { BrowserModule } from '@angular/platform-browser';
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AppRoutingModule } from './/app-routing.module';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { FormsModule } from '@angular/forms'

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { CreatorComponent } from './creator/creator.component';
import { ItemsComponent } from './items/items.component';
import { PageNotFoundComponent } from './pagenotfound/pagenotfound.component';
import { AboutComponent } from './about/about.component';
import { ItemService } from './items/items.service';
import { MagicItemService } from './magicitems/magicitems.service';
import { MagicItemsComponent } from './magicitems/magicitems.component';
import { MagicItemComponent } from './magicitem/magicitem.component'
import { ItemComponent } from './item/item.component';
import { HttpClientModule } from '@angular/common/http';

import { NavbarComponent } from './navbar/navbar.component';
import { PanelComponent } from './panel/panel.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    CreatorComponent,
    MagicItemsComponent,
    MagicItemComponent,
    ItemsComponent,
    ItemComponent,
    PageNotFoundComponent,
    AboutComponent,

    NavbarComponent,
    PanelComponent
  ],
  imports: [
    MDBBootstrapModule.forRoot(),
    RouterModule.forRoot([
      { path: "home", component: HomeComponent},
      { path: "creator", component: CreatorComponent},
      { path: "magicitems", component: MagicItemsComponent},
      { path: "magicitem/:id", component: MagicItemComponent},
      { path: "items", component: ItemsComponent},
      { path: "item/:id", component: ItemComponent},
      { path: "about", component: AboutComponent},
      { path: "", redirectTo: "home", pathMatch: "full"},
      { path: "**", component: PageNotFoundComponent},
    ], {useHash: true}),
    BrowserModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [
    ItemService,
    MagicItemService
  ],
  bootstrap: [AppComponent],
  schemas: [ NO_ERRORS_SCHEMA ]
})
export class AppModule { }
