import { Component } from '@angular/core';
import * as html2canvas from "html2canvas";
import { Element } from '@angular/compiler';
import { Title } from '@angular/platform-browser/src/browser/title';
import { IImageRootObject, MagicItemService } from '../magicitems/magicitems.service';
@Component({
  selector: 'creator-root',
  templateUrl: './creator.component.html',
  styleUrls: ['./creator.component.scss']
})
export class CreatorComponent {
  title: string = 'D&D 5e: Item Creator';
  constructor(private _svc:MagicItemService) { }
  public ItemName: string = '';
  public Description: string = '';
  public ItemRarity: string = '';
  public ItemCategory: string = '';
  window: Window;
  ngOnInit(){
  }
  update() : void {
    console.log('Called "update()"');
    var rarity = <HTMLSelectElement>document.getElementById("rarity");
    var itemType = <HTMLSelectElement>document.getElementById("itemType");
    this.ItemRarity = rarity.value;
    this.ItemCategory = itemType.value;
  }
  toImage() : void {
    console.log('Called "toImage()"');
    html2canvas(document.getElementById("WorkFrame")).then(function (canvas) {
      var imgData = canvas.toDataURL("image/png");
      var img = new Image();
      img.onload = function(){
        var imgElement = <HTMLImageElement>document.getElementById("result");
        imgElement.src = imgData;
      };
      img.src = imgData;
    });
  }
  upload() : void {
    console.log('Called "upload()"');
    let object:IImageRootObject = {
      Id: 0,
      name:this.ItemName,
      Name: this.ItemName,
      Rarity:this.ItemRarity,
      Category:this.ItemCategory,
      Description:this.Description,
      ImageURL:""
    }
    this._svc.postMagicItem(object);
  }
}