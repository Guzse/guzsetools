import { Component, OnInit } from '@angular/core';
import { NgSelectOption } from '@angular/forms';
import { identifierModuleUrl } from '@angular/compiler';
import { ItemService, IRootObject } from '../items/items.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.scss']
})
export class ItemComponent implements OnInit {
  title = 'Item';
  itemId: number = 1;
  itemData: IRootObject;
  private sub: any;

  constructor(private _svc: ItemService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.itemId = +params['id'];
   });
    this._svc.getItem(this.itemId).subscribe(result => {
      this.itemData = result;
      console.log(this.itemData);
      this.title = this.itemData.name;
    });
  }
}