import { Component, OnInit } from '@angular/core';
import { ItemService, IRootObject } from './items.service';
import { NgSelectOption } from '@angular/forms';
import { identifierModuleUrl } from '@angular/compiler';

@Component({
  selector: 'app-items',
  templateUrl: './items.component.html',
  styleUrls: ['./items.component.scss']
})
export class ItemsComponent implements OnInit {
  title = 'Items';
  items:IRootObject[] = [];
  filterItems:IRootObject[] = [];
  itemTypes:selectOption[] = [
    new selectOption(0,"All Items"),
    new selectOption(1,"Armor"),
    new selectOption(2,"Adventuring Gear"),
    new selectOption(3,"Mounts and Vehicles"),
    new selectOption(4,"Tools"),
    new selectOption(5,"Weapon")
  ];
  constructor(private _svc:ItemService) { }
  count: number = 256;

  itemName: string = "";
  itemType: string = "";

  ngOnInit() {
    this._svc.getItemSummary().subscribe(result => {
      this.count = result.count;
    });
    for (let i: number = 1; i <= this.count; i++) {
      this._svc.getItem(i).subscribe(result => {
        this.items[this.items.length] = result;
        console.log(this.items);
        this.filterItems = this.items;
        this.items.sort((a,b):number =>{
          if (a.index < b.index) return -1;
          if (a.index > b.index) return 1;
        });
      });
    }
  }
  toItem(i:number){
    console.log(i);
    debugger;
  }
  filter() {
    console.log("Filter: " + this.itemType);
    this.filterItems = [];
    for (let item of this.items){
      if (item.name.includes(this.itemName) && (item.equipment_category.includes(this.itemType) || this.itemType == "All Items")){
        this.filterItems[this.filterItems.length] = item;
      }
    }
    console.log(this.filterItems);
  }
}

export class selectOption {
  id: number;
  value: string;
  constructor(ID:number, Value:string){
    this.id = ID;
    this.value = Value;
  }
}