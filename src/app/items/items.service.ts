import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/do";
import "rxjs/add/observable/of";
import "rxjs/add/operator/map";

@Injectable()
export class ItemService {
    constructor(private _http: HttpClient) { };

    getItem(i: number): Observable<IRootObject> {
        return this._http.get<IRootObject>("http://www.dnd5eapi.co/api/equipment/" + i)
            .map(d => { d.weapon_category = d["weapon_category:"]; return d; })
        // .do(data => { console.log11(JSON.stringify(data)) });
    }

    getItemSummary(): Observable<ISummaryRootObject> {
        return this._http.get<ISummaryRootObject>("http://www.dnd5eapi.co/api/equipment/")
    }

    item: IRootObject;
    /*
    getVertrekkenWithCache(): Observable<IRootObject> {
        if (this.vertrekInfo)
            return Observable.of(this.vertrekInfo);
        else
            return this._http.get<IRootObject>("http://www.dnd5eapi.co/api/equipment/defects?limit=30")
                .do(data => { this.vertrekInfo = data; console.log(JSON.stringify(data)) });
    }
    */
}

export interface Cost {
    quantity: number;
    unit: string;
}

export interface DamageType {
    url: string;
    name: string;
}

export interface Damage {
    dice_count: number;
    dice_value: number;
    damage_type: DamageType;
}

export interface Range {
    normal: number;
    long?: any;
}

export interface Property {
    name: string;
    url: string;
}

export interface ArmorClass {
    base: number;
    dex_bonus: boolean;
    max_bonus?: any;
}

export interface Speed {
    quantity: number;
    unit: string;
}

export interface IRootObject {
    _id: string;
    index: number;
    name: string;
    equipment_category: string;
    "weapon_category:": string;
    weapon_category: string;
    weapon_range: string;
    category_range: string;
    armor_category: string;
    armor_class: ArmorClass;
    str_minimum: number;
    stealth_disadvantage: boolean;
    cost: Cost;
    tool_category: string;
    desc: string[];
    damage: Damage;
    gear_category: string;
    range: Range;
    weight: number;
    speed: Speed;
    vehicle_category: string;
    capacity: string;
    properties: Property[];
    url: string;
}

export interface IResult {
    name: string;
    url: string;
}

export interface ISummaryRootObject {
    count: number;
    results: IResult[];
}