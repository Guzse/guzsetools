import { Component, OnInit } from '@angular/core';
import { NgSelectOption } from '@angular/forms';
import { identifierModuleUrl } from '@angular/compiler';
import { MagicItemService, IImageRootObject } from '../magicitems/magicitems.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-item',
  templateUrl: './magicitem.component.html',
  styleUrls: ['./magicitem.component.scss']
})
export class MagicItemComponent implements OnInit {
  title = 'Magic Item';
  itemId: number = 1;
  itemData: IImageRootObject;
  private sub: any;

  constructor(private _svc: MagicItemService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.itemId = +params['id'];
   });
    this._svc.getMagicItem(this.itemId).subscribe(result => {
      this.itemData = result;
      console.log(this.itemData);
      //this line might give an error, but it's actually correct!
      this.title = this.itemData.name;
    });
  }
}