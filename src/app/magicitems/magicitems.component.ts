import { Component, OnInit } from '@angular/core';
import { MagicItemService, IImageRootObject } from './magicitems.service';
import { NgSelectOption } from '@angular/forms';
import { identifierModuleUrl } from '@angular/compiler';
import { Element } from '@angular/compiler';
import { Title } from '@angular/platform-browser/src/browser/title';

@Component({
  selector: 'app-items',
  templateUrl: './magicitems.component.html',
  styleUrls: ['./magicitems.component.scss']
})

export class MagicItemsComponent implements OnInit {
  title = 'Items';

  items:IImageRootObject;
  filterItems:IImageRootObject;
  
  public itemCategories:selectOption[] = [
    new selectOption(0,"All Items"),
    new selectOption(1,"Armor"),
    new selectOption(2,"Potion"),
    new selectOption(3,"Ring"),
    new selectOption(4,"Rod"),
    new selectOption(5,"Scroll"),
    new selectOption(6,"Staff"),
    new selectOption(7,"Wand"),
    new selectOption(8,"Weapon"),
    new selectOption(9,"Wondrous Item")
  ];
  public itemRarity:selectOption[] = [
    new selectOption(0,"Any Rarity"),
    new selectOption(1,"Common"),
    new selectOption(2,"Uncommon"),
    new selectOption(3,"Rare"),
    new selectOption(4,"Very Rare"),
    new selectOption(5,"Legendary")
  ]

  name:string;
  category:string;
  rarity:string;
  page:number = 0;
  
  constructor(private _svc:MagicItemService) { }
  ngOnInit(){
    this._svc.getAllMagicItems().subscribe(result =>{
      console.log(result);
      this.items = result;
      console.log(this.items);
    })
  }
  update(){
    this._svc.getAllMagicItems(this.page, this.name, this.category, this.rarity).subscribe(result =>{
      console.log(result);
      this.items = result;
      console.log(this.items);
    })
  }
  pageDown(){
    if (this.page != 0)
      this.page--;
    console.log(this.page)
    this.update()
  }
  pageUp(){
    if (true)
      this.page++;
    console.log(this.page)
    this.update();
  }
}

export class selectOption {
  id: number;
  value: string;
  constructor(ID:number, Value:string){
    this.id = ID;
    this.value = Value;
  }
}