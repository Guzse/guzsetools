import { Injectable } from "@angular/core";
import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/do";
import "rxjs/add/observable/of";
import "rxjs/add/operator/map";

@Injectable()
export class MagicItemService {
    constructor(private _http: HttpClient) { };

    getAllMagicItems(offset?:number, name?:string, category?:string, rarity?:string): Observable<IImageRootObject> {
        let url:string = "http://localhost:63514/api/v1/MagicItem?length=10";
        if (offset != null)
            url += "&page=" + offset;
        if (name != null)
            url += "&name=" + name;
        if (category != null)
            url += "&category=" + category;
        if (rarity != null)
            url += "&rarity=" + rarity;
        
        console.log("http.get("+url+")");
        return this._http.get<IImageRootObject>(url);
    }

    getMagicItem(id:number): Observable<IImageRootObject> {
        return this._http.get<IImageRootObject>("http://localhost:63514/api/v1/MagicItem/" + id);
    }

    postMagicItem(object:IImageRootObject){
        return this._http.post<IImageRootObject>("http://localhost:63514/api/v1/MagicItem", object).subscribe(res => {
            console.log(res);
          },
          (err: HttpErrorResponse) => {
            console.log(err.error);
            console.log(err.name);
            console.log(err.message);
            console.log(err.status);
            console.log(JSON.stringify(object));
          }
        );
    }
}

export interface IImageRootObject {
    Id: number;
    name: string;
    Name: string;
    Category: string;
    Rarity: string;
    Description: string;
    ImageURL: string;
}

export interface Cost {
    quantity: number;
    unit: string;
}

export interface DamageType {
    url: string;
    name: string;
}

export interface Damage {
    dice_count: number;
    dice_value: number;
    damage_type: DamageType;
}

export interface Range {
    normal: number;
    long?: any;
}

export interface Property {
    name: string;
    url: string;
}

export interface ArmorClass {
    base: number;
    dex_bonus: boolean;
    max_bonus?: any;
}

export interface Speed {
    quantity: number;
    unit: string;
}

export interface IRootObject {
    _id: string;
    index: number;
    name: string;
    equipment_category: string;
    "weapon_category:": string;
    weapon_category: string;
    weapon_range: string;
    category_range: string;
    armor_category: string;
    armor_class: ArmorClass;
    str_minimum: number;
    stealth_disadvantage: boolean;
    cost: Cost;
    tool_category: string;
    desc: string[];
    damage: Damage;
    gear_category: string;
    range: Range;
    weight: number;
    speed: Speed;
    vehicle_category: string;
    capacity: string;
    properties: Property[];
    url: string;
}

export interface IResult {
    name: string;
    url: string;
}

export interface ISummaryRootObject {
    count: number;
    results: IResult[];
}